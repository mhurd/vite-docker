import { defineConfig } from 'vite'

// Adds support for Vue3 SFC components
import vue from '@vitejs/plugin-vue'

// Allows you to use Markdown as Vue components and use your Vue components in Markdown files
import Markdown from 'vite-plugin-md'

// Enable use of Iconify icons ( https://icones.js.org ) w/o import
import Components from 'unplugin-vue-components/vite'
import Icons from 'unplugin-icons/vite'
import IconsResolver from 'unplugin-icons/resolver'

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    port: 3000,
  },
  plugins: [
    vue({
      include: [/\.vue$/, /\.md$/]
    }),
    Markdown(),
    Icons({ /* options */ }),
    Components({
      resolvers: [
        IconsResolver({
          prefix: 'icon'
        })
      ],
    })
  ]
})
