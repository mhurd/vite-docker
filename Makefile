SHELL := /bin/bash

.DEFAULT: freshinstall
.PHONY: freshinstall
freshinstall:
	make stop
	make remove
	make prepare
	make start

.PHONY: prepare
prepare:
	echo "Makefile prepare placeholder"
	docker compose up --no-start

.PHONY: remove
remove:
	-@read -p "Are you sure you want to delete vite-docker image and container? (y/n)? " -n 1 -r; \
	echo ; \
	if [ "$$REPLY" = "y" ]; then \
		docker compose down; \
		docker image rm vite-docker; \
	fi

.PHONY: start
start:
	( sleep 3; open http://localhost:3000 || xdg-open http://localhost:3000 || echo "Open 'http://localhost:3000' in a browser to view BrowserSync pages." ) &
	docker compose up

.PHONY: stop
stop:
	-docker compose down

.PHONY: restart
restart:
	make stop
	make start

.PHONY: sh
sh:
	docker exec -it vite-docker sh